#!/bin/bash
SRC=./csv
TARGET=./db
DATABASE_FILE=basic_fantasy.sqlite3
DATABASE_PATH="${TARGET}/${DATABASE_FILE}"

# Remove any existing database files
rm -rf "${DATABASE_PATH}"

# Iterate through csv files
FILES=$(ls "${SRC}")
for i in $FILES
do
    # Indicate the file we are working on
    echo "Importing ${SRC}/${i}"
    CSV_FILE="${SRC}/${i}"
    TABLE="${i%.*}"
    echo -e ".separator ","\n.import ${CSV_FILE}" "${TABLE}"| sqlite3 "${DATABASE_PATH}"
done
