# Basic Fantasy

## About this Repository

This repository contains lookup tables and supporting scripts for the 
popular role-playing system known as Basic Fantasy.

## What is Basic Fantasy?

![Basic Fantasy|256x256,20%](basic-fantasy-large-color-logo.png "Basic Fantasy") 

The Basic Fantasy Role-Playing Game is a rules-light game system modeled 
on the classic RPG rules of the early 1980's. Though based loosely on the
d20 SRD v3.5, Basic Fantasy RPG has been written largely from scratch to 
replicate the look, feel, and mechanics of the early RPG game systems. 
It is suitable for those who are fans of "old-school" game mechanics. 
Basic Fantasy RPG is simple enough for children in perhaps second or 
third grade to play, yet still has enough depth for adults as well.

For more information, visit https://basicfantasy.org/

## Usage 

## Disclaimer

Note that the author of this repository, while a long-time fan of classic 
"subterranean medieval prisons and ancient mythical winged reptiles"-
inspired role-playing games, is not associated with the aforementioned 
Basic Fantasy system.

While every effort is made by contributors to ensure that accurate information 
is disseminated through this medium, the contributors make no representation 
about the suitability of the content of this repository for any purpose.

It is provided "as is" without express or implied warranty. Use at your own risk!

